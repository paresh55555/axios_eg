import React, { Component }  from 'react';
import './App.css';
import axios from 'axios';
import TableData from './table';
import {Table} from "reactstrap";
import data from "./data";
//const API_URL = 'https://jsonplaceholder.typicode.com/todos/2';
const API_URL = data;

function App() {
  return (
    <div className="App">
      <TableData/>
    </div>
  );
}

// class App extends Component {
//     state = {
//         users: [data]
//     }
//     componentDidMount() {
//         const url = `${API_URL}`;
//         axios.get(url).then(response => response.data)
//             .then((data) => {
//                 this.setState({ users: data })
//                 console.log(this.state.users)
//             })
//     }
//     // [...]
//     TableData() {
//         return this.state.users.map((user) => {
//             // const { id, companyName, address, email } = detail;
//             return (
//                 <tr>
//                     <h5 className="card-title">{user.name}</h5>
//                     <h6 className="card-subtitle mb-2 text-muted">
//                         {user.email}
//                     </h6>
//                     <td>{user.name}</td>
//
//                     <td>{user.email}</td>
//                 </tr>
//             )
//         })
//     }
//     render() {
//
//         return (
//             <div className="container">
//                 <div className="col-xs-8">
//                     <h1>React Axios Example</h1>
//                     <Table striped>
//                         <thead>
//                         <tr>
//                             <th>SR No.</th>
//                             <th>Company Name</th>
//                             <th>Address</th>
//                             <th>Email id</th>
//                         </tr>
//                         </thead>
//                         <tbody>
//                         {this.TableData()}
//                         </tbody>
//                     </Table>
//                     {this.state.users.map((user) => (
//                         <div className="card">
//                             <div className="card-body">
//                                 <h5 className="card-title">{user.name}</h5>
//                                 <h6 className="card-subtitle mb-2 text-muted">
//                                     {user.email}
//                                 </h6>
//                             </div>
//                         </div>
//                     ))}
//                 </div>
//             </div>
//         );
//     }
// }

export default App;
