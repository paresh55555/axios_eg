import React, { Component } from 'react'
import { Table } from 'reactstrap';
import data from "./data";

class TableData extends Component {
    constructor(props) {
        super(props);
        this.state = data
    }

    TableData() {
         return this.state.map((detail) => {
           // const { id, companyName, address, email } = detail;
            return (
                <tr>
                    <td>{detail.id}</td>
                    <td>{detail.companyName}</td>
                    <td>{detail.address}</td>
                    <td>{detail.email}</td>
                </tr>
            )
        })
    }

    render() {
        return (
            <div>
                <Table striped>
                    <thead>
                    <tr>
                        <th>SR No.</th>
                        <th>Company Name</th>
                        <th>Address</th>
                        <th>Email id</th>
                    </tr>
                    </thead>
                    <tbody>
                     {this.TableData()}
                    </tbody>
                </Table>
                <div>
                    <p>

                    </p>
                </div>
            </div>

        )
    }
}

export default TableData;